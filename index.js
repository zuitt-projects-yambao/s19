let student1 = {
	name 		: "Shawn Michaels",
	birthday 	: "May 5, 2003",
	age 		: 18,
	isEnrolled	: true,
	classes		: ["Philosphy 101", "Social Sciences 201"]
}

let student2 = {
	name 		: "Steve Austin",
	birthday	: "June 15, 2001",
	age 		: 20,
	isEnrolled	: true,
	classes		: ["Philosphy 401", "Natural Sciences 402"]
}

const{name, birthday, age, isEnrolled, classes} = student1;
	//Note: You can destructure objects inside functions.

	// console.log("Hi! " + "I'm " + student.name + " ." + " I am " + student.ages + " years old.");
	console.log(`Hi! I'm ${name}. I am ${age} years old`);
	// console.log("I study the following courses + " classes);
	console.log(`I study the following courses ${classes}.`);




// function getCube(num){

// 	console.log(num ** 3);

// }

// let cube = getCube(3);


// const greet = (person) => {
// 	console.log(`Hi ${person.name}`);
// };
// console.log(cube)

const getCube = (num) => num ** 3;
let cube = getCube(3);
console.log(cube);




let numArr = [15,16,32,21,21,2]

// numArr.forEach = num => {

// 	console.log(num);
// }

numArr.forEach(num =>{
	console.log(num);
});




let numsSquared = numArr.map(num =>{

	return num ** 2;

})

console.log(numsSquared);



class Dog {
	constructor(name, breed, dogAge){
		this.name = name;
		this.breed  = breed;
		this.dogAge  = dogAge * 7;

	};
};

let dog1 = new Dog("Kulet", "Askal", "10");

console.log(dog1);

let dog2 = new Dog("Suplado", "Askal", "12");

console.log(dog2);
